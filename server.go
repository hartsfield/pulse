// pulse is meant to be used as a starting point for new custom servers
package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/hartsfield/fd"
	"gitlab.com/hartsfield/gmailer"
)

// Incoming requests to a server should create a Context, and outgoing calls to
// servers should accept a Context.
// TODO:
//       signup/signin and 3 level auth
func main() {
	// Any package you import, directly or through other dependencies, has access
	// to http.DefaultServeMux and might register routes you don’t expect,
	// because of this we define our own serve mux.
	// (https://blog.gopheracademy.com/advent-2016/exposing-go-on-the-internet/)
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(home))
	mux.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))

	// Server configuration
	srv := &http.Server{
		Addr:              ":443",
		Handler:           mux,
		ReadHeaderTimeout: 5 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       5 * time.Second,
	}

	// Start accepting connections
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			fmt.Println(err)
		}
		// if err := srv.ListenAndServeTLS("./auth/cert.pem", "./auth/key.pem"); err != nil {
		// 	fmt.Println(err)
		// }
	}()

	fmt.Println("Server started @ " + srv.Addr)

	// Create a channel to listen for an interrupt and trigger a graceful shutdown
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	// The server will immediately exit unless we use this channel as a blocking
	// mechanism to wait for the interrupt signal (ctrl+c)
	// NOTE: You won't see logging if you run this from within vim
	<-stopChan
	fmt.Println("Received interrupt signal, halting server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := srv.Shutdown(ctx)
	if err != nil {
		fmt.Println("ERROR shutting down server: ", err)
		return
	}
	fmt.Println("Server halted gracefully")
}

// Use an init function to begin monitoring the number of open file descriptors
func init() {
	fdc := &fd.Fdcount{Interval: 10, MaxFiles: 200000}
	fdc.Start(catchOverflow)
}

// catcatchOverflow is the function called when we go over the max number of
// open file descriptors.
func catchOverflow(numOpenFiles int) {
	fmt.Println(numOpenFiles, "open file descriptors")
	msg := gmailer.Message{
		Recipient: "anonymous15415@gmail.com",
		Subject:   "ALERT! Server has exceeded the maximum number of open file descriptors",
		Body:      "...",
	}
	msg.Send()
}

// Home function with sample code
func home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello world!"))
	/* or
	data, err := ioutil.ReadFile("./public/tmpl/home.tmpl")
	handleErr(err)
	w.Write(data)
	*/
}
